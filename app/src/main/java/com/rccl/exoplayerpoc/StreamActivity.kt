package com.rccl.exoplayerpoc

import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.TextureView
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.hls.DefaultHlsDataSourceFactory
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_stream.*

class StreamActivity : AppCompatActivity() {
    companion object {
        const val EXTRA_URL = "url"
    }

    private lateinit var exoPlayer: SimpleExoPlayer
    private lateinit var url: String
    private val handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stream)
        url = intent.getStringExtra(EXTRA_URL)
        createAndPrepareExoPlayer()
        surface_stream.player = exoPlayer
        surface_stream.useController = false
        updateVideo()
    }

    private fun createAndPrepareExoPlayer() {
        val defaultBandwidthMeter = DefaultBandwidthMeter()
        exoPlayer = ExoPlayerFactory.newSimpleInstance(this, DefaultTrackSelector(AdaptiveTrackSelection.Factory(defaultBandwidthMeter)))
        val dataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, packageName), defaultBandwidthMeter)
        val factory = HlsMediaSource.Factory(DefaultHlsDataSourceFactory(dataSourceFactory))
        exoPlayer.prepare(factory.createMediaSource(Uri.parse(url)))
    }

    private fun updateVideo() {
        handler.post {
            with(surface_stream.videoSurfaceView as TextureView) {
                if (bitmap != null) {
                    image_stream.setImageBitmap(bitmap)
                }
            }
            updateVideo()
        }
    }

    override fun onPause() {
        super.onPause()
        exoPlayer.playWhenReady = false
        exoPlayer.playWhenReady
    }

    override fun onResume() {
        super.onResume()
        exoPlayer.playWhenReady = true
        exoPlayer.playWhenReady
    }
}
