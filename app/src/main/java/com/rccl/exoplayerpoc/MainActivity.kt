package com.rccl.exoplayerpoc

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    companion object {
        private const val DEFAULT_URL = "https://mnmedias.api.telequebec.tv/m3u8/29880.m3u8"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        edit_main_url.setText(DEFAULT_URL)
        button_main_play.setOnClickListener {
            if (checkUrl()) {
                startStream()
            } else {
                showEmptyError()
            }
        }
    }

    private fun checkUrl() = !getUrlFromUi().isEmpty()

    private fun startStream() = openActivity(StreamActivity::class.java, { it.putExtra(StreamActivity.EXTRA_URL, getUrlFromUi()) })

    private fun getUrlFromUi() = edit_main_url.text.toString()

    private fun showEmptyError() = Toast.makeText(this, getString(R.string.error_main_url), Toast.LENGTH_LONG).show()
}

fun AppCompatActivity.openActivity(target: Class<*>, intentInterceptor: ((Intent) -> Unit)? = null) {
    val intent = Intent(this, target)
    intentInterceptor?.invoke(intent)
    this.startActivity(intent)
}
